# Mapa conceptual 1
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .caf {
    BackgroundColor #FFCAA6
  }
  .your_style_name {
    BackgroundColor lightblue
  }
}
</style>
* Von-Neumann vs Hardvard node <<rose>>
	* Hardvard
		* modelo harvard node <<green>>
			* toma su nombre de la computadora harvard mark
			*_ utilizaba dos memorias
				* una de las memorias contenían las instrucciones
				* otra contenía la colección de datos
			* buses
				* son independientes
					* pueden contener diferente información en la \nmisma ubicación de las diferentes memorias
			* memoria central
				* se encuentra almacenada en la información tanto \ndel programacomo el conjunto de datos
	* Von-Neumann físico matemático
		* 1956 logró recibir el premio enrico fermi node <<green>>
		* 30 de julio de 1945 node <<green>>
			* desarrolló un programa que consistía en \nel almacenado en memoria de los datos
		* nació el 28 de diciembre de 1903 node <<green>>
			* Y murió el 8 de febrero de 1957
		* la adopción del bit como medida de la memoria en los ordenadores 0 y 1 node <<green>>
		* creo la arquitectura de los computadores actuales node <<green>>
			* almacenamiento
				* lugar en el que se almacena tanto los datos como instrucciones node <<your_style_name>>
					*_ existen dos grandes categorías
						* almacenamiento a largo plazo
							* es decir cualquier información que \nse guarda por un periodo de tiempo largo
								* como el disco duro
						* almacenamiento a corto plazo
			* cpu
				* sistema operativo node <<your_style_name>>
					* es el que le da las instrucciones al al procesador \npara que sea éste quien opere todas esas instrucciones
				* este es el núcleo central del computador node <<your_style_name>>
					* se encarga directamente de ejecutar las aplicaciones \nque recibe por parte del sistema operativo
						* se divide en dos grandesáreas
							* la unidad aritmético lógica
								* se encarga específicamente de realizar las operaciones que tienen \nque ver con el procesamiento de todos los datos
							* unidad de control
								* se encarga precisamente junto con un reloj de establecer la cantidad \nde tiempo de procesamiento que lleva cada una de las aplicaciones
			* los periféricos  de entrada y salida
				* entrada node <<your_style_name>>
					* la información entra
						* por ejemplo
							*_ el teclado
							*_ mouse
							*_ microfono
				* salida node <<your_style_name>>
					* la información sale
						* por ejemplo
							*_ impresora
							*_ monitor
							*_ altavoces
				* mixtos node <<your_style_name>>
					* cumplen con las dos funciones
						* por ejemplo
							*_ memoria usb
							*_ fax
			* buses
				* es el conexionado que permite la comunicación \nentre los distintos bloques funcionales del sistema node <<your_style_name>>
					* son comúnmente de cobre o de algún tipo de aleación
					* llevan y traen información por medio depulsaciones eléctricas
						* se pueden dividir en tres tipos
							* buses de datos de direcciones node <<caf>>
								* lo que hace es precisamente mandar información acerca de dónde está localizada \nla información o en qué posición de la memoria se encuentra esa información
							* buses de control node <<caf>>
								* tiene la finalidad de que todo de alguna forma tenga un cierto orden y que se \ncontrole los envío y recepción de información que se emite
							* buses de datos node <<caf>>
								* circular información a través de los diferentes elementos
@endmindmap
```

# Mapa conceptual 2
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .naranja {
    BackgroundColor #FF5733
  }
}
</style>
* Supercomputadoras en México node <<naranja>>
	* hace ya 51 años que comenzó en México La era de la computación node <<rose>>
		* 1958 el 18 de junio para ser preciso es el día que significativamente se da este fenómeno
		* UNAM tuvo laprimera gran computadora en Latinoamérica eso fue en 1958
			*_ la IBM 650
				* tenía una memoriade 2 k
				* leía la información a través de tarjetas perforadas
					* era capaz de procesar 1000 operaciones aritméticas por segundo
						* su principal uso en la investigación científica
	* en 1991 nuestra UNAM se consolida en las grandes ligas de la computación node <<rose>>
		*_ cray 432
			* equivalía a 2000 computadoras de oficina
	* kanbalan node <<rose>>
		* realizar siete millones de operaciones Matemáticas por segundo
		* con una capacidad equivalente a más de 1300 computadoras de escritorio
			* con esto podemos realizar en tan solo una semana \ncálculos que de otra forma requerida más de 25 años
				* se utiliza para realizar proyectos de investigación en áreas como \nastronomía química cuántica ciencia de materiales la tecnología diseño de fármacos ciencias genómicas
	* una supercomputadora es una infraestructura que se diseña y construye para procesar \ngrandes cantidades de datos o información de una manera mucho más rápida que otros equipos de cómputo node <<rose>>
	* miztli node <<rose>>
		* capacidad a 8 mil 344 procesadores
		* predecir el clima estudiar los efectos de los sismos diseñar nuevos materiales \nfármacos y muchas otras investigaciones son realizados por la computadora
		* la misión número uno de una supercomputadora es de alguna forma sustituir lo que \nson laboratorios tradicionales de investigación científica para que por medio de modelos matemáticos
			* realiza 228 millones esto es millones de millones \nde operaciones de punto flotante en un solo segundo
			* contribuye entre otros temas al estudio del universo de sismos y \ndel comportamiento de partículas subatómicas así como el diseño de nuevos materiales fármacos y reactores nucleares
	* supercomputadora de BUAP node <<rose>>
		* uno de sus principales usos es en el sector Automotriz y aeronáutica \nporque aquí se puede hacer la simulación de qué pasa con algunos materiales cuando un vehículo va a cierta velocidad
		* tieneuna capacidad de almacenamiento equivalente a casi 5000 computadoras portátiles
			* tan soloun segundo puede ejecutar 2000 millones de operaciones que superaciones
	* xiuhcoat node <<rose>>
		* cual surge de una iniciativa de las tres instituciones más importantes de \neducación superior de crear un proyecto denominado laboratorio nacional de cómputo de alto desempeño
			* se caracterisa por sus nodos robustos de supercómputo en los centros \nde datos de cada una de estas tres instituciones
@endmindmap
```


